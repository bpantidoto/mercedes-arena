﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using EasyWiFi.Core;
using System;

using MoreMountains.HighroadEngine;

namespace EasyWiFi.ServerControls
{
    public class AccelerateButtonHandler : MonoBehaviour, IServerController
    {
        public string[] controls;

        public EasyWiFiConstants.PLAYER_NUMBER player = EasyWiFiConstants.PLAYER_NUMBER.Player1;
        
        [Tooltip("Determines when your Notify Method gets called")]
        public EasyWiFiConstants.CALL_TYPE callType = EasyWiFiConstants.CALL_TYPE.Every_Frame;

        //runtime variables
        ButtonControllerType[] button1 = new ButtonControllerType[EasyWiFiConstants.MAX_CONTROLLERS];
        ButtonControllerType[] button2 = new ButtonControllerType[EasyWiFiConstants.MAX_CONTROLLERS];

        ButtonControllerType[][] buttons;

        int currentNumberControllers = 0;
        bool lastValue = false;

        void OnEnable()
        {
            EasyWiFiController.On_ConnectionsChanged += checkForNewConnections;

            buttons = new ButtonControllerType[][] {button1, button2};

            //do one check at the beginning just in case we're being spawned after startup and after the callbacks
            //have already been called
            for(int i = 0; i< controls.Length; i++)
            {
                if (buttons[i][0] == null && EasyWiFiController.lastConnectedPlayerNumber >= 0)
                {
                    EasyWiFiUtilities.checkForClient(controls[i], (int)player, ref buttons[i], ref currentNumberControllers);
                }
            }
        }

        void OnDestroy()
        {
            EasyWiFiController.On_ConnectionsChanged -= checkForNewConnections;
        }

        // Update is called once per frame
        void Update()
        {
            //iterate over the current number of connected controllers
            for(int j = 0; j<buttons.Length; j++)
            {
                for (int i = 0; i < currentNumberControllers; i++)
                {
                    if (buttons[j][i] != null && buttons[j][i].serverKey != null && buttons[j][i].logicalPlayerNumber != EasyWiFiConstants.PLAYERNUMBER_DISCONNECTED)
                    {
                        mapDataStructureToAction(i);
                    }                    
                }
            }
        }

        public void mapDataStructureToAction(int index)
        {
            int playerValue = (int)player;
            int value = 0;

            if(buttons[0][index].BUTTON_STATE_IS_PRESSED)
            {
                value = 1;
            }

            if(buttons[1][index].BUTTON_STATE_IS_PRESSED)
            {
                value = -1;
            }

            InputManager.Instance.HandleWifiInputVertical(playerValue, value);
        }

        public void checkForNewConnections(bool isConnect, int playerNumber)
        {
           for(int i = 0; i< controls.Length; i++)
            {
                EasyWiFiUtilities.checkForClient(controls[i], (int)player, ref buttons[i], ref currentNumberControllers);
            }
        }
    }
}