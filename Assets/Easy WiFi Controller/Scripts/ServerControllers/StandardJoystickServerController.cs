﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using EasyWiFi.Core;
using System;

using MoreMountains.HighroadEngine;

namespace EasyWiFi.ServerControls
{

    [AddComponentMenu("EasyWiFiController/Server/UserControls/Standard Joystick")]
    public class StandardJoystickServerController : MonoBehaviour, IServerController
    {
        public string control = "Joystick1";
        public EasyWiFiConstants.PLAYER_NUMBER player = EasyWiFiConstants.PLAYER_NUMBER.Player1;

        public float sensitivity = .01f;

        //runtime variables
        JoystickControllerType[] joystick = new JoystickControllerType[EasyWiFiConstants.MAX_CONTROLLERS];
        int currentNumberControllers = 0;
        
        

        void OnEnable()
        {
            EasyWiFiController.On_ConnectionsChanged += checkForNewConnections;

            //do one check at the beginning just in case we're being spawned after startup and after the callbacks
            //have already been called
            if (joystick[0] == null && EasyWiFiController.lastConnectedPlayerNumber >= 0)
            {
                EasyWiFiUtilities.checkForClient(control, (int)player, ref joystick, ref currentNumberControllers);
            }
        }

        void OnDestroy()
        {
            EasyWiFiController.On_ConnectionsChanged -= checkForNewConnections;
        }

        // Update is called once per frame
        void Update()
        {
            //iterate over the current number of connected controllers
            for (int i = 0; i < currentNumberControllers; i++)
            {
                if (joystick[i] != null && joystick[i].serverKey != null && joystick[i].logicalPlayerNumber != EasyWiFiConstants.PLAYERNUMBER_DISCONNECTED)
                {
                    mapDataStructureToAction(i);
                }
            }
        }
        
        Vector3 actionVector3;
        float horizontal;
        float vertical;
        public void mapDataStructureToAction(int index)
        {
            horizontal = joystick[index].JOYSTICK_HORIZONTAL * sensitivity;
            //vertical = joystick[index].JOYSTICK_VERTICAL * sensitivity;
            //actionVector3 = EasyWiFiUtilities.getControllerVector3(horizontal, vertical, joystickHorizontal, joystickVertical);
            //if(horizontal != 0)
            //{
                //ConnectionStatusManager.Instance.customMessage = "player: "+((int)player).ToString()+ " value: "+horizontal; 
            //}
            //InputManager.Instance.HandleWifiInputHorizontal(index, horizontal);
            InputManager.Instance.HandleWifiInputHorizontal((int)player, horizontal);

            /* 
            if(ConnectionStatusManager.Instance.playersValues.Count > (int)player)
            {
                ConnectionStatusManager.Instance.playersValues[(int)player] = horizontal;
            } 
            */           
        }

        public void checkForNewConnections(bool isConnect, int playerNumber)
        {
            EasyWiFiUtilities.checkForClient(control, (int)player, ref joystick, ref currentNumberControllers);
        }
    }
}

