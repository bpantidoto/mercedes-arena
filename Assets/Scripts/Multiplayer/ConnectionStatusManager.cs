﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using EasyWiFi.Core;
using EasyWiFi.ServerControls;

using MoreMountains.HighroadEngine;

public class ConnectionStatusManager : MonoBehaviour 
{
	#region SINGLETON PATTERN
	public static ConnectionStatusManager _instance;
	public static ConnectionStatusManager Instance
	{
		get {
			if (_instance == null)
			{
				_instance = GameObject.FindObjectOfType<ConnectionStatusManager>();
				
				if (_instance == null)
				{
					GameObject container = new GameObject("[ConnectionStatusManager]");
					_instance = container.AddComponent<ConnectionStatusManager>();
				}
			}
			return _instance;
		}
	}
	#endregion

	private bool _messageWaiting = false;
    private int _messagePlayerNumber = -1;
    private bool _messageIsConnect = false;
	private bool _justConnected = false;

	private string _customMessage = "";
	public string customMessage {
		get {
			return _customMessage;
		}
		set {
			_customMessage = value;
		}
	}

	private bool _buttonPressed = false;
	public bool buttonPressed {
		get {
			return _buttonPressed;
		}
		set {
			_buttonPressed = value;
		}
	}

	private int _controllersConnected = 0;
	public int controllersConnected {
		get {
			return _controllersConnected;
		}
	}

    private int _playersReady = 0;
	public int playersReady {
		get {
			return _playersReady;
		}
	}

	public List<float> playersValues = new List<float>();

	private LocalLobbyPlayerUI[] _playersUI;

	private void Awake() 
	{
		if(ConnectionStatusManager.Instance != null && ConnectionStatusManager.Instance != this)
		{
			GameObject.Destroy(gameObject);
			return;
		}
		_instance = this;
		DontDestroyOnLoad(gameObject);
		Cursor.visible = false;

#if UNITY_EDITOR
	Cursor.visible = true;
#endif
	}

	private void Start() 
	{
		_playersUI = GameObject.FindObjectsOfType<LocalLobbyPlayerUI>();
	}

	private void Update() 
	{
		if (_justConnected)
            _justConnected = false;
		else if (_messageWaiting)
			HandlePlayerConnection();
	}

	private void OnEnable()
    {
        EasyWiFiController.On_ConnectionsChanged += OnConnectionsChange;
    }

    private void OnDestroy()
    {
        EasyWiFiController.On_ConnectionsChanged -= OnConnectionsChange;
    }

	private void OnConnectionsChange(bool isConnect, int playerNumber)
	{
		if (playerNumber < 0 || playerNumber > 5)
        {
            return;
        }
		
		foreach(LocalLobbyPlayerUI current in _playersUI)
		{
			if(current.Position == playerNumber)
			{
				//current.MainAction();
				break;
			}
		}

		_messageIsConnect = isConnect;
        _messagePlayerNumber = playerNumber;
        _messageWaiting = true;
	}

	private void HandlePlayerConnection()
	{
		//check to make sure this wasn't a disconnection
		if (_messageIsConnect)
        {
            if (!_justConnected)
            {
				_controllersConnected++;
				playersValues.Add(0);
			}
		}
		else
        {
            //it actually was a disconnect
			_controllersConnected--;
		}

		//reset the instance variables
        _messageWaiting = false;
        _messageIsConnect = false;
        _messagePlayerNumber = -1;
	}

	private void CheckForReadyPlayers()
	{
		if (_playersReady == 0 || _playersReady < _controllersConnected)
		{
			//startGameButton.endBlink();
		}

		if (_playersReady > 0 && _playersReady == _controllersConnected)
		{
			//startGameButton.startBlink();
		}
	}
}
