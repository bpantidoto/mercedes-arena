﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConnectionStatusText : MonoBehaviour 
{
	private Text _textfield;

	private string _text;

	private void Awake() 
	{
		_textfield = GetComponent<Text>();
		_text = _textfield.text;
	}

	private void Update()
	{
		_textfield.text = GetFormatedText();
	}
	
	private string GetFormatedText()
	{
		string finalString = string.Format(_text, ConnectionStatusManager.Instance.controllersConnected);
		
		return finalString;
	}
}
