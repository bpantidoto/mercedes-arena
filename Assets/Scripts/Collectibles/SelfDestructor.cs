﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestructor : MonoBehaviour 
{
	private void Update() 
	{	
		if(transform.childCount == 0)
		{
			GameObject.Destroy(gameObject);
			return;
		}
	}	
}
