﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MoreMountains.HighroadEngine;

public class CollectibleSpawner : MonoBehaviour 
{
	public Transform bottomLeft;
	public Transform bottomRight;
	public Transform topLeft;
	public Transform topRight;
	public GameObject collectiblePrefab;

	public float newSpawnInterval = 1000f;
	public int maxSpawns = 5;
	public float height = 0;
	public float collectibleSpawnRadiusCheckSize = 10f;

	private List<Collectible> _collectibles;
	public List<Collectible> collectibles{
		get {
			return _collectibles;
		}
	}

	private ArenaManager _arenaManager;

	public CustomEvent onSpawn;
	public CustomEvent onDespawn;

	private void Start() 
	{
		_collectibles = new List<Collectible>();
		_arenaManager = GetComponent<ArenaManager>();
	}

	float currentTime = 0f;
	private void Update() 
	{
		if(_collectibles.Count < maxSpawns)
		{
			currentTime += Time.deltaTime;
			if(currentTime > newSpawnInterval)
			{
				currentTime = 0;
				GameObject newBorn = GameObject.Instantiate(collectiblePrefab, 
					new Vector3(Random.Range(bottomLeft.position.x, bottomRight.position.x), height, Random.Range(bottomRight.position.z, topRight.position.z)),
					Quaternion.identity) as GameObject;

				Collectible collectible = newBorn.GetComponent<Collectible>();
				collectible.spawner = this;
				
				RepositionCollectible(collectible);

				_collectibles.Add(collectible);

				onSpawn.Invoke();
			}
		}
	}

	private void RepositionCollectible(Collectible collectible)
	{
		Vector3 positionToInstantiate;
		Collider[] hitColliders;
		
		bool remake = false;
		do
		{
			positionToInstantiate = new Vector3(Random.Range(bottomLeft.position.x, bottomRight.position.x), height, Random.Range(bottomRight.position.z, topRight.position.z));
			hitColliders = Physics.OverlapSphere(positionToInstantiate, collectibleSpawnRadiusCheckSize);
			remake = false;
			foreach(Collider current in hitColliders)
			{
				if(current.gameObject.tag == Constants.TAG_RAMP)
				{
					remake = true;
				}
			}
		} while (remake);

		collectible.transform.position = positionToInstantiate;
	}

	public void Collected(Collectible collectible, GameObject car)
	{
		if(_collectibles.Contains(collectible))
		{
			_collectibles.Remove(collectible);
		}
		
		GameObject.Destroy(collectible.gameObject);

		onDespawn.Invoke();
	}	
}


