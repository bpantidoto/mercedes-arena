﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawner : MonoBehaviour 
{
    public float timeToRespawn = 10f;
    private float _currentTime = 0;
    private bool _shouldRespawn = false;

    public GameObject prefab;


    private void Start() 
    {
#if UNITY_EDITOR
        timeToRespawn = 2f;
#endif
    }

	private void Update() 
    {
        if(_shouldRespawn)
        {
            _currentTime += Time.deltaTime;
            if(_currentTime >= timeToRespawn)
            {
                _currentTime = 0;
                RespawnObject();
            }
        }

        _shouldRespawn = (transform.childCount == 0);
    }

    private void RespawnObject()
    {
        GameObject newBorn = GameObject.Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
        newBorn.transform.SetParent(transform);
        newBorn.transform.localPosition = new Vector3(0,0,0);
    }
}
