﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.HighroadEngine;
using DG.Tweening;

public class CollectibleGrowSize : MonoBehaviour 
{
	/* 
	
	public CollectibleSpawner spawner;
	*/
	protected SoundManager _soundManager;

	public AudioClip CollectSound;

	private bool _canCollide = true;

	private Transform _target;
	private Vector3 _originalScale;

	public Vector3 sizeGrow;

	private void Awake() 
	{
		_soundManager = FindObjectOfType<SoundManager>();
	}

	private void Start() 
	{
		transform.DORotate(new Vector3(0,359,0),1.5f,RotateMode.LocalAxisAdd).SetLoops(-1);	
	}

	private void OnTriggerEnter(Collider other) 
	{	
		if(_canCollide)
		{
			_target = other.transform;
			_originalScale = _target.localScale;

			CarFXController fx = _target.GetComponent<CarFXController>();
			if(fx != null)
			{
				fx.PlaySizeBoost();
			}

			StartCoroutine(GrowInSize());

			Renderer rend = GetComponent<Renderer>();
			if(rend != null)
			{
				rend.enabled = false;
			}

			for(int i = 0; i< transform.childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(false);
			}

			_canCollide = false;
		}
	}

	private IEnumerator GrowInSize()
	{
		yield return new WaitForSeconds(0.4f);
		_soundManager.PlaySound(CollectSound, transform.position, true);
		_target.DOScale(sizeGrow, 0.5f).SetEase(Ease.OutBounce);
		GameObject coroutineHolder = new GameObject("[Coroutine]");
		TemporaryCoroutine coroutine = coroutineHolder.AddComponent<TemporaryCoroutine>();
		coroutine.duration = 15f;
		coroutine.customAction += FinishSizeBuff;
		coroutine.Comence();
	}

	private void FinishSizeBuff()
	{
		_target.DOScale(_originalScale, 0.5f).SetEase(Ease.InBounce).OnComplete(SelfDestruct);
	}

	private void SelfDestruct()
	{
		transform.DOKill();
		GameObject.Destroy(gameObject);
	}
}
