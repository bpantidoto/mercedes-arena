﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarFXController : MonoBehaviour 
{
	public GameObject speeedboost;
	public GameObject sizeboost;
	public GameObject textPopup;

	public void PlaySpeedBoost()
	{
		speeedboost.gameObject.SetActive(true);
	}

	public void PlaySizeBoost()
	{
		sizeboost.gameObject.SetActive(true);
	}

	public void PlayTextPopup(int value)
	{
		string formated = string.Format("+{0}",value);
		GameObject newborn = GameObject.Instantiate(textPopup, Vector3.zero, Quaternion.identity) as GameObject;
		newborn.transform.parent = transform;
		newborn.transform.localPosition = Vector3.zero;

		PopupText popup = newborn.GetComponent<PopupText>();
		popup.SetTextAndComence(formated);
	}
}
