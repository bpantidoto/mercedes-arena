﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.HighroadEngine;
using DG.Tweening;

public class Collectible : MonoBehaviour 
{
	public AudioClip CollectSound;
	public CollectibleSpawner spawner;
	public int value;
	
	protected SoundManager _soundManager;
	private bool _shouldTriggerEnter = true;

	private void Awake() 
	{
		_soundManager = FindObjectOfType<SoundManager>();

		if(value == 0)
		{
			value = Constants.SPEEDBOOST_VALUE;
		}
	}

	private void Start() 
	{
		transform.DORotate(new Vector3(0,359,0),1.5f,RotateMode.LocalAxisAdd).SetLoops(-1);	
	}

	private void OnTriggerEnter(Collider other) 
	{	
		if(_shouldTriggerEnter)
		{
			_soundManager.PlaySound(CollectSound, transform.position, true);
			_shouldTriggerEnter = false;

			transform.DOKill();

			Renderer rend = gameObject.GetComponent<Renderer>();
			if(rend != null)
			{
				rend.enabled = false;
			}

			for(int i = 0; i< transform.childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(false);
			}

			CarFXController fx = other.GetComponent<CarFXController>();
			//value 0 means its a speedboost
			if(fx != null)
			{
				if(value == Constants.SPEEDBOOST_VALUE)
				{
					fx.PlaySpeedBoost();
				}else{
					fx.PlayTextPopup(value);
				}
			}	
		}
	}

	private void OnTriggerExit(Collider other) 
	{
		if(value > 0)
		{
			BaseController carController = other.GetComponent<BaseController>();
			if(carController == null)
			{
				carController = other.GetComponentInParent<BaseController>();
			}

			if(carController != null)
			{
				carController.ArenaScore += value;
			}
		}

		if(spawner != null)
		{
			spawner.Collected(this, other.gameObject);
		}else{
			GameObject.Destroy(gameObject);
		}
	}
}
