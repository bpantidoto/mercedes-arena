﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Orbiter : MonoBehaviour 
{
	public float displacementValue = 14f;
	public float velocity = 2f;

	private Vector3 _initialPos;
	private int _factor = 1;

	private void Awake() 
	{
		_initialPos = new Vector3(-displacementValue,0,0);	
	}

	private IEnumerator Start() 
	{
		yield return new WaitForSeconds(0.1f);
		AssignNewTween();	
	}

	private void AssignNewTween()
	{
		if(transform.localPosition.x < 0)
		{
			MoveRight();
		}else{
			MoveLeft();
		}
	}

	private void MoveLeft()
	{
		transform.DOLocalMove(new Vector3((displacementValue*-1), 0, 0), 1.5f).SetEase(Ease.OutSine).OnComplete(AssignNewTween);
	}

	private void MoveRight()
	{
		transform.DOLocalMove(new Vector3(displacementValue, 0, 0), 1.5f).SetEase(Ease.OutSine).OnComplete(AssignNewTween);
	}

	private void OnDestroy() 
	{
		transform.DOKill();
	}
}
