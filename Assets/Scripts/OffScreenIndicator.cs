﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OffScreenIndicator : MonoBehaviour 
{
	private Camera _camera;
	private List<Indicator> _indicators;
	private Vector3 _screenPos;
	private Vector2 _onScreenPos;
	private float _max = 10f;

	public float offSetValue = 30f;
	public List<CollectibleSpawner> _spawnersToWatch;
	public GameObject indicatorPrefab;
	public Transform indicatorsPanel;

	private void Awake () 
	{
		_indicators = new List<Indicator>();
		_camera = Camera.main;
	}

	public void OnSpawnNewTarget() 
	{
		foreach(CollectibleSpawner spawner in _spawnersToWatch)
		{
			foreach(Collectible current in spawner.collectibles)
			{
				if(IsNotDuplicate(current.gameObject))
				{
					CreateIndicator(current.gameObject);
				}
			}
		}
	}

	public bool IsNotDuplicate(GameObject target)
	{
		foreach(Indicator indicator in _indicators)
		{
			if(indicator.PointToTarget == target.gameObject)
			{
				return false;
			}
		}
		return true;
	}

	public void OnDespawnTarget()
	{
		for(int i = _indicators.Count-1; i>-1; i--)
		{
			if(_indicators[i] != null)
			{
				if(_indicators[i].PointToTarget == null)
				{
					Indicator current = _indicators[i];
					_indicators.Remove(_indicators[i]);
					GameObject.Destroy(current);
				}
			}
		}
	}

	private void CreateIndicator(GameObject pointToTarget)
	{
		GameObject newBorn = GameObject.Instantiate(indicatorPrefab, Vector3.zero, Quaternion.identity) as GameObject;
		Indicator indicator = newBorn.AddComponent<Indicator>();
		indicator.SetIndicatorTarget(pointToTarget);
		_indicators.Add(indicator);
		indicator.transform.SetParent(indicatorsPanel);
	}

	private void Update () 
	{
		foreach (var obj in _indicators)
        {
			if(obj.PointToTarget != null)
			{
				Image image = obj.GetComponent<Image>();
				RectTransform rect = obj.GetComponent<RectTransform>();

				_screenPos = _camera.WorldToViewportPoint(obj.PointToTarget.transform.position);
				_onScreenPos = new Vector2(_screenPos.x-0.5f, _screenPos.y-0.5f)*2; 
				_max = Mathf.Max(Mathf.Abs(_onScreenPos.x), Mathf.Abs(_onScreenPos.y));
				_onScreenPos = (_onScreenPos/(_max*2))+new Vector2(0.5f, 0.5f);

				Vector3 indicatorPosition = new Vector3(Screen.width * _onScreenPos.x, Screen.height * _onScreenPos.y, 0);
				float angle = GetAngle(indicatorPosition);

				rect.position = GetOffsetPosition(indicatorPosition);
				rect.rotation = Quaternion.AngleAxis(angle, Vector3.forward);	

				if(_screenPos.x >= 0 && _screenPos.x <= 1 && _screenPos.y >= 0 && _screenPos.y <= 1)
				{
					//object is inside camera view
					image.enabled = false;
					return;
				}
				image.enabled = true;
			}	
        }
	}

	private Vector3 GetOffsetPosition(Vector3 position)
	{
		Vector3 screenCenter = new Vector3(Screen.width*0.5f, Screen.height*0.5f, 0);
		float xOffset = 0f;
		float yOffset = 0f;

		//top right
		if(position.x > screenCenter.x && position.y > screenCenter.y)
		{
			xOffset = offSetValue*-1;
			yOffset = offSetValue*-1;
		}

		//top left
		if(position.x < screenCenter.x && position.y > screenCenter.y)
		{
			xOffset = offSetValue;
			yOffset = offSetValue*-1;
		}

		//bottom left
		if(position.x < screenCenter.x && position.y < screenCenter.y)
		{
			xOffset = offSetValue;
			yOffset = offSetValue;
		}

		//bottom right
		if(position.x > screenCenter.x && position.y < screenCenter.y)
		{
			xOffset = offSetValue*-1;
			yOffset = offSetValue;
		}

		return new Vector3(position.x + xOffset, position.y + yOffset, position.z);
	}

	private float GetAngle(Vector3 target)
    {
		Vector3 targetDir = target - new Vector3(Screen.width*0.5f, Screen.height*0.5f,0);
		float angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg;
		return angle;
    }
}


