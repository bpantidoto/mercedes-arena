﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Indicator : MonoBehaviour
{
	public GameObject Target { get; private set; }
	public GameObject PointToTarget { get; private set; }

	public void SetIndicatorTarget(GameObject pointToTarget)
	{
		Target = gameObject;
        PointToTarget = pointToTarget;
	}

	private void Update()
	{
		if(PointToTarget == null)
		{
			GameObject.Destroy(gameObject);
		}
	}
}
