﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using MoreMountains.HighroadEngine;
using MoreMountains.Tools;
using EasyWiFi.ServerControls;

public class ArenaManager : MonoBehaviour 
{	
	protected bool _isPlaying;
	protected float _currentGameTime; 

	public SoundPlayer soundPlayer;

	[Header("Start positions")]
	public Transform[] StartPositions; 
	public int StartGameCountDownTime = 3; 
	public bool NoCollisions = false;
	public int maxPlayTime = 60;
	public Dictionary<int, BaseController> Players { get; protected set;}

	[Header("Test Mode")]
	public GameObject[] TestHumanPlayers;
	protected bool _testMode = false;
	
	[Header("Camera")]
	public CameraController cameraController;

	public UnityAction OnDisableControlForPlayers;
	public UnityAction OnEnableControlForPlayers;

	[Header("UI")]
	public Text StartGameCountdown; 
	public RectTransform EndGamePanel; 
	public Text EndGameRanking;
	public Button BackToMenuButton; 
	public Text ScoreText1;
	public Text ScoreText2;
	public Text ScoreText3;
	public Text TimerText;

	public GameObject[] trophies;

	public virtual void Awake()
	{
		if (StartPositions.Length == 0)
			Debug.LogWarning("List of StartPositions is empty. You should provide at least one start position");

		Application.targetFrameRate = 300;
	}

	private void Start() 
	{
		Players = new Dictionary<int, BaseController>();

		OnDisableControlForPlayers = DisableControlForPlayers;
		OnEnableControlForPlayers = EnableControlForPlayers;

		if (LocalLobbyManager.Instance.Players().Count == 0)
		{
			InitTestMode();
		}

		HandleUIStartup();
		ManagerStart();
	}

	private void ManagerStart()
	{
		InitPlayers();
		UpdateNoPlayersCollisions();
		OnDisableControlForPlayers();
		StartCoroutine(StartGameCountdownCoroutine());
	}

	protected virtual void InitPlayers()
	{
		List<Transform> cameraHumanTargets = new List<Transform>();
		List<Transform> cameraBotsTargets = new List<Transform>();

		int _currentStartPosition = 0;
		List<ILobbyPlayerInfo> playersAtStart;
		playersAtStart = new List<ILobbyPlayerInfo>(LocalLobbyManager.Instance.Players().Values);
		
		if (playersAtStart.Count > StartPositions.Length)
		{
			Debug.LogWarning("You only have "
			+ StartPositions.Length
			+ " start positions in your scene but have "
			+ playersAtStart.Count
			+ " players ready to start. Either remove players or add more Start positions in your scene.");
		} 
		else
		{
			foreach (ILobbyPlayerInfo item in playersAtStart)
			{
				GameObject prefab;

				if (item.VehicleSelectedIndex >= 0)
				{
					prefab = LocalLobbyManager.Instance.AvailableVehiclesPrefabs[item.VehicleSelectedIndex];
				} 
				else
				{
					prefab = Resources.Load("vehicles/" + item.VehicleName) as GameObject;
				}

				GameObject newPlayer = Instantiate(
											prefab,
											StartPositions[_currentStartPosition].position, 
											Quaternion.Euler(new Vector3(0, StartPositions[_currentStartPosition].rotation.y, 0))
										) as GameObject;

				BaseController car = newPlayer.GetComponent<BaseController>();
				Players[item.Position] = car;
				car.name = item.Name;

				cameraHumanTargets.Add(newPlayer.transform);

				_currentStartPosition++;
			}
		}

		cameraController.BotPlayers = cameraBotsTargets.ToArray();
		cameraController.HumanPlayers = cameraHumanTargets.ToArray();
	}

	private void HandleUIStartup()
	{
		if (EndGamePanel != null)
			EndGamePanel.gameObject.SetActive(false);

		if (ScoreText1 != null)
			ScoreText1.text = "";

		if (ScoreText2 != null)
			ScoreText2.text = "";

		if (ScoreText3 != null)
			ScoreText3.text = "";

		if(TimerText != null)
			TimerText.text = "";

		if (BackToMenuButton != null)
		{
			BackToMenuButton.onClick.RemoveAllListeners();
			BackToMenuButton.onClick.AddListener(ReturnToMenu);
		}
	}

	public virtual void Update() 
	{
		if(Input.GetKeyDown(KeyCode.Escape))
			ReturnToMenu();

		if(_isPlaying) 
		{
			_currentGameTime += Time.deltaTime;
			if (EndGamePanel != null)
			{
				var playersRank = UpdatePlayersList();	

				playersRank.Sort(
					delegate(BaseController p1, BaseController p2) 
					{
						int compare = -p1.ArenaScore.CompareTo(p2.ArenaScore);
						return compare;
					}
				);

				if (playersRank.Count > 0)
				{
					if (ScoreText1 != null && ScoreText2 != null && ScoreText3 != null)
					{
						string newscore1 = "";
						string newscore2 = "";
						string newscore3 = "";
						int position = 1;

						foreach (var p in playersRank)
						{
							newscore1 += position + "\r\n";
							newscore2 += string.Format("| {0}\r\n",
								p.name
							);
							newscore3 += string.Format("{0}\r\n", 
								p.ArenaScore
							);
							position++;
						}

						ScoreText1.text = newscore1;
						ScoreText2.text = newscore2;
						ScoreText3.text = newscore3;
					}

					if(TimerText != null)
					{
						float currentTime = Mathf.Round(maxPlayTime - _currentGameTime);
						float minutes = Mathf.Floor(currentTime / 60);
						float seconds = Mathf.RoundToInt(currentTime%60);
						string minutesString = minutes.ToString();
						string secondsString = seconds.ToString();

						if(minutes < 10) {
							minutesString = "0" + minutes.ToString();
						}
						if(seconds < 10) {
							secondsString = "0" + Mathf.RoundToInt(seconds).ToString();
						}

						TimerText.text = minutesString + ":" + secondsString;
					}

					if (_currentGameTime >= maxPlayTime && !_testMode)
					{
						OnDisableControlForPlayers();

						_isPlaying = false;

						ShowFinalRanking(playersRank);

						AddEndGameInputController();
					}
				}
			}
		}
		
		CheckIfPlayersNeedRespawn();
	}

	private void CheckIfPlayersNeedRespawn()
	{
		for(int i =0; i<Players.Count; i++)
		{
			BaseController control;
			if(Players.TryGetValue(i, out control))
			{
				Transform playerTransform = control.transform;
			
				if(playerTransform.rotation.x >= 0.45f || playerTransform.rotation.x <= -0.45f || playerTransform.rotation.z >= 0.45f || playerTransform.rotation.z <= -0.45f )
				{
					RespawnPlayer(i);
					break;	
				}
			}
		}
	}

	private void RespawnPlayer(int controllerID)
	{
		Players[controllerID].DisableControls(controllerID);

		Transform carTransform = Players[controllerID].transform;
		carTransform.position = StartPositions[controllerID].position;
		carTransform.rotation = Quaternion.Euler(new Vector3(0, StartPositions[controllerID].rotation.y, 0));

		Players[controllerID].EnableControls(controllerID);

		carTransform.GetComponent<CarFXController>().PlaySizeBoost();
	}

	protected virtual void ShowFinalRanking(List<BaseController> playersRank)
	{
		foreach(GameObject current in trophies)
		{
			current.SetActive(false);
		}

		string finalRank = "\n";//"--- "+ playersRank[0].name + " ganhou com: " + playersRank[0].ArenaScore.ToString() + " pontos. ---\n";

		if(playersRank.Count > 1)
		{
			if(playersRank[0].ArenaScore == playersRank[1].ArenaScore)
			{
				finalRank = "--- "+ playersRank[0].name + " e " + playersRank[1].name + " empataram! ---\n";
				GameObject newTrophy = GameObject.Instantiate(trophies[0], Vector3.zero, Quaternion.identity) as GameObject;
				newTrophy.transform.SetParent(trophies[0].transform.parent);

				RectTransform trophy = newTrophy.GetComponent<RectTransform>();

				trophy.position = trophies[1].GetComponent<RectTransform>().position;
				trophy.localScale = trophies[1].GetComponent<RectTransform>().localScale;

				trophies[1].SetActive(false);
				newTrophy.gameObject.SetActive(true);
			}
		}

		for (int i = 0; i < playersRank.Count; i++) 
		{
			finalRank += "\r\n" + Constants.GetPlayerName(playersRank[i].name) + " fez " + playersRank[i].ArenaScore + " pontos.";
			if(i < trophies.Length)
			{
				trophies[i].SetActive(true);
			}
		}

		ShowEndGameScreen(finalRank);
	}

	protected virtual void ShowEndGameScreen(string text)
	{
		EndGameRanking.text = text;
		EndGamePanel.gameObject.SetActive(true);
		soundPlayer.PlayVictorySound();
	}

	protected virtual List<BaseController> UpdatePlayersList()
	{
		List<BaseController> cars = new List<BaseController>();
		foreach(var car in Players.Values)
		{
			cars.Add(car);
		}
		return cars;
	}

	public virtual void ReturnToMenu() 
	{
		if (_testMode)
		{
			Debug.LogWarning("In Test Mode, you can't quit current scene.");
			return;
		}

		LocalLobbyManager.Instance.ReturnToLobby();
	}

	private void InitTestMode()
	{
		_testMode = true;

		int currentPosition = 0;

		if (TestHumanPlayers != null)
		{
			foreach (var player in TestHumanPlayers)
			{
				if (player.GetComponent<BaseController>() != null)
				{
					LocalLobbyManager.Instance.AddPlayer(new LocalLobbyPlayer {
						Position = currentPosition,
						Name = player.GetComponent<VehicleInformation>().LobbyName + (currentPosition + 1),
						VehicleName = player.name,
						VehicleSelectedIndex = -1,
						IsBot = false
					});
					currentPosition++;
				}
				else 
				{
					Debug.LogWarning(player.name + " can't be instantiated for test mode. Test mode needs a prefab with a BaseController Component.");
				}
			}
		}
	}

	public virtual IEnumerator StartGameCountdownCoroutine()
	{
		soundPlayer.PlayStartGame();
		UpdateCountdownText("COMEÇANDO EM:\n" + 3);
		yield return new WaitForSeconds(1f);
		if (EndGamePanel != null)
		{
			float remainingCountDown = StartGameCountDownTime;

			while (remainingCountDown > 1)
			{
				yield return null;

				remainingCountDown -= Time.deltaTime;
				int newFloorTime = Mathf.FloorToInt(remainingCountDown);
				UpdateCountdownText("COMEÇANDO EM:\n" + newFloorTime);
			}
		}
		_isPlaying = true;
		_currentGameTime = 0f;
		OnEnableControlForPlayers();
		UpdateCountdownText("VAI!");
		soundPlayer.PlayBackgroundMusic();
		yield return new WaitForSeconds(1);
		UpdateCountdownText("");
	}

	public virtual void UpdateCountdownText(string text)
	{
		if (StartGameCountdown == null)
			return;

		if (text == "")
		{
			StartGameCountdown.gameObject.SetActive(false);
		}
		else 
		{
			StartGameCountdown.text = text;
		}
	}

	public virtual void UpdateNoPlayersCollisions()
	{
		if (NoCollisions)
		{
			int vehiclesLayer = LayerMask.NameToLayer("Actors");
			Physics.IgnoreLayerCollision(vehiclesLayer, vehiclesLayer);
		}
	}

	public virtual void EnableControlForPlayers() 
	{
		foreach(KeyValuePair<int, ILobbyPlayerInfo> keyValuePair in LocalLobbyManager.Instance.Players())
		{
			if (Players.ContainsKey(keyValuePair.Key)) 
			{
				int controllerID = keyValuePair.Key;
				Players[controllerID].EnableControls(controllerID);
			}
		}
	}

	public virtual void DisableControlForPlayers() 
	{
		foreach(KeyValuePair<int, ILobbyPlayerInfo> keyValuePair in LocalLobbyManager.Instance.Players())
		{
			if (Players.ContainsKey(keyValuePair.Key)) 
			{
				int controllerID = keyValuePair.Key;
				Players[controllerID].DisableControls(controllerID);
			}
		}
	}

	private void AddEndGameInputController()
	{
		gameObject.GetComponent<EndGameInputController>().enabled = true;
		gameObject.GetComponent<CustomButtonServerController>().enabled = true;
	}
}
