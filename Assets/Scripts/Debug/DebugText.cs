﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using  EasyWiFi.ServerControls;
using  EasyWiFi.Core;

public class DebugText : MonoBehaviour 
{
	public Text debugText;

	public CustomButtonServerController buttonServerController;
	public StandardDpadServerController dpadServerController;

	#region SINGLETON PATTERN
	public static DebugText _instance;
	public static DebugText Instance
	{
		get {
			if (_instance == null)
			{
				_instance = GameObject.FindObjectOfType<DebugText>();
				
				if (_instance == null)
				{
					GameObject container = new GameObject("DebugTextManager");
					_instance = container.AddComponent<DebugText>();
				}
				
			}
		
			return _instance;
		}
	}
	#endregion

	private string _displayText;

	private void Awake() 
	{
		if(_instance == null)
		{
			_instance = this;
		}
		if(debugText == null)
		{
			debugText = GameObject.Find("DebugText").GetComponent<Text>();
		}
	}

	public void SetText(string theText)
	{
		debugText.text = theText;
	}

	public void AppendText(string append)
	{

	}
}
