﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetScene : MonoBehaviour 
{
	private void Start() 
	{
		GameObject[] taggedWithReset = GameObject.FindGameObjectsWithTag(Constants.TAG_RESET);

		foreach(GameObject current in taggedWithReset)
		{
			GameObject.Destroy(current);
		}
		StartCoroutine(LoadLobbyScene());
	}

	private IEnumerator LoadLobbyScene()
	{
		yield return new WaitForSeconds(3f);
		SceneManager.LoadScene(0);
	}
}

