﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.HighroadEngine;

public class SoundPlayer : MonoBehaviour 
{
	public static SoundPlayer instance;

	public AudioClip VictorySoundClip;
	public AudioClip BackgroundSoundClip;
	public AudioClip StartGameSoundClip;
	public AudioClip CrashSoundClip;

	public bool AutoPlayBackgroundMusic = true;

	private AudioSource _backgroundAudioSource;
	private SoundManager _soundManager;

	public delegate void OnCompleteDelegate();

	private bool _crashed = false;
	private float _currentCrashTimer = 0f;
	public float minimumCrashDelay = 0.5f;

	private void Awake() 
	{
		instance = this;

		_backgroundAudioSource = gameObject.AddComponent<AudioSource>() as AudioSource;	
		_backgroundAudioSource.playOnAwake = false;
		_backgroundAudioSource.spatialBlend = 0;
		_backgroundAudioSource.rolloffMode = AudioRolloffMode.Logarithmic;
		_backgroundAudioSource.loop = true;
		_backgroundAudioSource.clip=BackgroundSoundClip;
		
		_soundManager = FindObjectOfType<SoundManager>();
	}

	private void Start() 
	{
		if(AutoPlayBackgroundMusic)
		{
			PlayBackgroundMusic();
		}
	}

	private void Update() 
	{
		if(Input.GetKeyDown(KeyCode.P))
		{
			PlayVictorySound();
		}
		IncreaseCrashTime();
	}

	public void PlayVictorySound()
	{
		if (_soundManager == null)
		{
			Debug.LogWarning("Need a SoundManager gameObject in the scene to play music. Please add one.");
			return;
		}
		
		_soundManager.StopBackGroundMusic();
		_soundManager.PlayStinger(VictorySoundClip, Camera.main.transform.position);
	}

	public void PlayBackgroundMusic() 
	{
		if (_soundManager == null)
		{
			Debug.LogWarning("BackgroundMusic need a SoundManager gameObject in the scene to play music. Please add one.");
			return;
		}
		
		_soundManager.PlayBackgroundMusic(_backgroundAudioSource);
	}

	public void PlayStartGame(OnCompleteDelegate onComplete = null)
	{
		if (_soundManager == null)
		{
			Debug.LogWarning("Need a SoundManager gameObject in the scene to play music. Please add one.");
			return;
		}
		
		_soundManager.StopBackGroundMusic();
		AudioSource source = _soundManager.PlayStinger(StartGameSoundClip, Camera.main.transform.position);
		if(onComplete != null)
		{
			StartCoroutine(OnCompleteCoroutine(onComplete, source.clip.length));
		}
	}

	private IEnumerator OnCompleteCoroutine(OnCompleteDelegate onComplete, float delay)
	{
		yield return new WaitForSeconds(delay);
		onComplete();
	}

	public void PlayCrashSound()
	{
		if(!_crashed)
		{
			_soundManager.PlayLowerVolumeSFX(CrashSoundClip, transform.position);
		}
	}

	private void IncreaseCrashTime()
		{
			if(_crashed)
			{
				_currentCrashTimer += Time.deltaTime;
				if(_currentCrashTimer >= minimumCrashDelay)
				{
					_crashed = false;
				}
			}
		}
}
