﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PopupText : MonoBehaviour 
{
	public Text textfield;
	private Vector3 _desiredScale;

	public Vector3 tweenToPosition = new Vector3(0,4,0);
	public float durationIn = 0.7f;
	public float durationOut = 0.7f;

	private void Awake() 
	{
		_desiredScale = transform.localScale;
		transform.localScale = new Vector3(0,0,0);
	}
	
	private void Update() 
	{
		transform.LookAt(Camera.main.transform);
	}
	
	public void SetTextAndComence(string text)
	{
		textfield.text = text;
		AnimateIn();
	}

	private void AnimateIn()
	{
		transform.DOScale(_desiredScale, durationIn).SetEase(Ease.OutQuad);
		transform.DOLocalMove(tweenToPosition, durationIn).SetEase(Ease.OutExpo).OnComplete(AnimateOut);
	}

	private void AnimateOut()
	{
		transform.DOScale(new Vector3(0,0,0), durationOut).SetEase(Ease.InQuad).OnComplete(SelfDestruct).SetDelay(0.5f);
	}

	private void SelfDestruct()
	{
		GameObject.Destroy(gameObject);
	}
}
