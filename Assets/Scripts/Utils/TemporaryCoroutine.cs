﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TemporaryCoroutine : MonoBehaviour 
{
	public UnityAction customAction;
	public float duration = 1f;

	public void Comence() 
	{
		StartCoroutine(Coroutine());
	}

	private IEnumerator Coroutine()
	{
		yield return new WaitForSeconds(duration);
		if(customAction != null)
		{
			customAction.Invoke();
		}
		GameObject.Destroy(gameObject);
	}
}
