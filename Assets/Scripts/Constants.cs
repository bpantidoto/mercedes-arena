﻿public class Constants 
{
	public static readonly string[] CAR_COLORS = {"yellow", "red", "white", "green", "blue", "black"};
	public static readonly string[] BR_CAR_COLORS = {"Amarelo", "Vermelho", "Cinza", "Verde", "Azul", "Preto"};

	public const string TAG_RAMP = "Ramp";
	public const string TAG_RESET = "Reset";


	public const string PLAYER_YELLOW = "<color=#f9aa00>{0}</color>";
	public const string PLAYER_RED = "<color=#cf0101>{0}</color>";
	public const string PLAYER_WHITE = "<color=#c1d5e7>{0}</color>";
	public const string PLAYER_GREEN = "<color=#359460>{0}</color>";
	public const string PLAYER_BLUE = "<color=#6B9DFFFF>{0}</color>";


	public const int SPEEDBOOST_VALUE = 5;


	public static string GetPlayerName(string name)
	{
		string playername = "";

		switch(name)
		{
			case("Amarelo"):
				playername = string.Format(PLAYER_YELLOW,name);
			break;
			case("Vermelho"):
				playername = string.Format(PLAYER_RED,name);
			break;
			case("Cinza"):
				playername = string.Format(PLAYER_WHITE,name);
			break;
			case("Verde"):
				playername = string.Format(PLAYER_GREEN,name);
			break;
			case("Azul"):
				playername = string.Format(PLAYER_BLUE,name);
			break;
		}

		return playername;
	}
}
