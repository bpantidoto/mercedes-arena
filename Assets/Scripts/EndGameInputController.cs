﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.HighroadEngine;

public class EndGameInputController : MonoBehaviour, IActorInput 
{
	private void Start() 
	{
		foreach(KeyValuePair<int, ILobbyPlayerInfo> keyValuePair in LocalLobbyManager.Instance.Players())
		{
			int controllerID = keyValuePair.Key;
			InputManager.Instance.SetPlayer(controllerID, this);
		}
	}

	#region IPlayerInput implementation

	private bool _canPress = true;
	public virtual void MainActionDown()
	{
		if(_canPress)
		{
			StartCoroutine(OnMainAction());
		}
	}

	public IEnumerator OnMainAction()
	{
		_canPress = false;
		yield return new WaitForSeconds(0.5f);
		SceneManager.LoadScene("Reset");
		_canPress = true;
	}	

	public virtual void MainActionReleased() { }
	public virtual void AltActionDown(){ }
	protected bool okToMove;
	public virtual void HorizontalPosition(float value) { }
	public virtual void VerticalPosition(float value) { }
	public void RespawnActionDown() { }
	public virtual void AltActionReleased() { }
	public virtual void RespawnActionReleased() { }
	public void MobileJoystickPosition(Vector2 value) { }
	public virtual void LeftPressed() { }
	public virtual void RightPressed() { }
	public virtual void UpPressed() { }
	public virtual void DownPressed() { }
	public void AltActionPressed() { }
	public void RespawnActionPressed() { }
	public virtual void MainActionPressed() { }
	public void LeftReleased() { }
	public void RightReleased() { }
	public void UpReleased() { }
	public void DownReleased() { }

	#endregion
}