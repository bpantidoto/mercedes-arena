﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.HighroadEngine 
{
	/// <summary>
	/// Modifies the scene's gravity based on the multiplier parameter
	/// This allows for fine tuning of the vehicle's physics
	/// By default the value is 1
	/// </summary>
	public class GravityManager : MonoBehaviour 
	{
		[Range(1,5)]
		/// Gravity multiplier factor
		public int GravityMultiplier = 1;

		/// <summary>
		/// At awake we update the gravity based on our multiplier
		/// </summary>
		public virtual void Awake()
		{
			Physics.gravity = Physics.gravity * GravityMultiplier;
		}
	}
}
