﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using EasyWiFi.ServerBackchannels;
using UnityEngine.SceneManagement;

namespace MoreMountains.HighroadEngine 
{
	public class LocalLobbyPlayerUI : MonoBehaviour, IActorInput 
	{
		[Header("Slot data")]
		public int Position;
		public string JoinActionInputName;
		public LocalLobbyGameUI MenuSceneManager;

		[Header("GUI Elements")]
		public Text PlayerNameText;
		public RectTransform AddPlayerZone;
		public Button NewPlayerButton;
		public RectTransform ChooseVehicleZone;
		public Button RemovePlayerButton;
		public Button LeftButton;
		public Button RightButton;
		public Text VehicleText;
		public Image VehicleImage;
		public Button ReadyButton;
		public Sprite ReadyButtonSprite;
		public Sprite CancelReadyButtonSprite;

		protected LocalLobbyManager _localLobbyManager;
		protected bool _ready; 
		protected bool _isBot;
		protected int _currentVehicleIndex = -1;

		private bool _canToggle = true;
		private float _toggleDelay = 0.5f;
		private StringServerBackchannel _backchanel;

		public virtual void Start() 
		{
			InitManagers();

			InputManager.Instance.SetPlayer(Position, this);

			InitStartState();

			_backchanel = GetComponent<StringServerBackchannel>();
			_backchanel.setValue("none");
		}

		protected virtual void InitManagers()
		{
			_localLobbyManager = LocalLobbyManager.Instance;
		}

		protected virtual void InitStartState()
		{
			PlayerNameText.text = Constants.BR_CAR_COLORS[Position];
		
			NewPlayerButton.GetComponentInChildren<Text>().text = "Esperando\nJogador";

			_ready = false;
			
			ChooseVehicleZone.gameObject.SetActive(false);

			if (_localLobbyManager.ContainsPlayer(Position)) 
			{
				ILobbyPlayerInfo player = _localLobbyManager.GetPlayer(Position);
				_currentVehicleIndex = player.VehicleSelectedIndex;
				PlayerNameText.text = player.Name;

				if (player.IsBot)
				{
					OnAddBotButton();
				}
				else
				{
					OnAddPlayerButton();
					_localLobbyManager.RemovePlayer(Position);
				}
			}

			if (InputManager.Instance.MobileDevice)
			{
				if (Position == 0) 
				{			
					if (AddPlayerZone.gameObject.activeSelf)
					{
						OnAddPlayerButton();
					}
				}
				else 
				{
					NewPlayerButton.gameObject.SetActive(false);
				}
			}

			ShowSelectedVehicle();
		}

		public void SetPlayerNameAndUpdateUI(string playerName)
		{
			if (_localLobbyManager.ContainsPlayer(Position)) 
			{
				_localLobbyManager.UpdatePlayerName(playerName, Position);
				ILobbyPlayerInfo player = _localLobbyManager.GetPlayer(Position);
				PlayerNameText.text = player.Name;
			}
		}

		protected virtual void ShowSelectedVehicle() 
		{
			if (_currentVehicleIndex != -1)
			{
				VehicleInformation info = _localLobbyManager.AvailableVehiclesPrefabs[_currentVehicleIndex].GetComponent<VehicleInformation>();

				VehicleText.text = info.LobbyName;
				VehicleImage.sprite = info.lobbySprite;

				_localLobbyManager.registerLobbyPlayer(Position, _currentVehicleIndex);
			}
		}

		public virtual void OnAddPlayerButton() 
		{
			AddLobbyPlayer(false);
		}
		
		public virtual void OnAddBotButton() 
		{
			AddLobbyPlayer(true);
		}

		protected virtual void AddLobbyPlayer(bool isBot)
		{
			_isBot = isBot;

			//HAXXXOR
			_currentVehicleIndex = Position;

			ShowSelectedVehicle();

			ChooseVehicleZone.gameObject.SetActive(true);

			if (_isBot)
			{
				ILobbyPlayerInfo player = _localLobbyManager.GetPlayer(Position);
				PlayerNameText.text = player.Name;

				ReadyButton.gameObject.SetActive(false);

				AddLocalPlayerToLobby();
			}
			else 
			{
				PlayerNameText.text = Constants.BR_CAR_COLORS[Position];
				CancelPlayer();
			}
		}

		public virtual void OnRemovePlayer() 
		{
			ChooseVehicleZone.gameObject.SetActive(false);
			_localLobbyManager.RemovePlayer(Position);
			_localLobbyManager.unregisterLobbyPlayer(Position);
			_currentVehicleIndex = -1;
			PlayerNameText.text = Constants.BR_CAR_COLORS[Position];
		}

		public virtual void OnLeft() 
		{
			if(_canToggle && this != null)
			{
				//StartCoroutine(ToggleVehicleOrMapLeft());
			}
		}
		
		public virtual void OnRight() 
		{
			if(_canToggle && this != null)
			{
				//StartCoroutine(ToggleVehicleOrMapRight());
			}
		}

		private IEnumerator ToggleVehicleOrMapLeft()
		{
			_canToggle = false;
			if (Position == 0 && _ready) 
			{
				MenuSceneManager.OnLeft();
			}else{
				if (_currentVehicleIndex == 0)
				{
					_currentVehicleIndex = _localLobbyManager.AvailableVehiclesPrefabs.Length - 1;
				} 
				else 
				{
					_currentVehicleIndex -= 1;
				}

				ShowSelectedVehicle();
			}			
			yield return new WaitForSeconds(_toggleDelay);
			_canToggle = true;
		}

		private IEnumerator ToggleVehicleOrMapRight()
		{
			_canToggle = false;
			if (Position == 0 && _ready) 
			{
				MenuSceneManager.OnRight();
			}else{
				if (_currentVehicleIndex == (_localLobbyManager.AvailableVehiclesPrefabs.Length - 1)) 
				{
					_currentVehicleIndex = 0;
				} 
				else 
				{
					_currentVehicleIndex += 1;
				}

				ShowSelectedVehicle();
			}			
			yield return new WaitForSeconds(_toggleDelay);
			_canToggle = true;
		}
		
		public virtual void OnReady(bool fromGUI) 
		{
			if (!_ready) 
			{
				LeftButton.gameObject.SetActive(false);
				RightButton.gameObject.SetActive(false);
				RemovePlayerButton.gameObject.SetActive(false);
				_ready = true;

				AddLocalPlayerToLobby();
			} 
			else 
			{
				if (!fromGUI)
				{
					if (Position == 0 && _ready && _localLobbyManager.CanBegin())
					{
						MenuSceneManager.OnStartGame();
						return;
					}
				}	
			}
		}

		protected virtual void AddLocalPlayerToLobby()
		{
			LocalLobbyPlayer p = new LocalLobbyPlayer {
				Position = Position,
				Name = _isBot ? VehicleText.text : PlayerNameText.text,
				VehicleName = VehicleText.text,
				VehicleSelectedIndex = _currentVehicleIndex,
				IsBot = _isBot
			};
			_localLobbyManager.AddPlayer(p);

			_backchanel.setValue(Constants.CAR_COLORS[_currentVehicleIndex]);
		}
		
		protected virtual void CancelPlayer()
		{
			_ready = false;
			string buttonText = "";

			if (_isBot)
			{
				buttonText = "Bot Ready?";
			}
			else 
			{
				buttonText = "Pronto ?";
			}

			_localLobbyManager.RemovePlayer(Position);
		}

#region IPlayerInput implementation
		public virtual void MainActionDown()
		{
			if(_canToggle)
			{
				StartCoroutine(OnMainAction());
			}
		}
		public virtual void MainActionReleased() { }

		public IEnumerator OnMainAction()
		{
			_canToggle = false;
			MainAction();
			yield return new WaitForSeconds(_toggleDelay);
			_canToggle = true;
		}

		public void MainAction()
		{
			if (!ChooseVehicleZone.gameObject.activeSelf) 
			{
				OnAddPlayerButton();
				OnReady(false);
			} 
			else 
			{
				OnReady(false);	
			}			
		}
		
		public virtual void AltActionDown()
		{
			if (_ready)
			{
				CancelPlayer();
			}
			else if (ChooseVehicleZone.gameObject.activeSelf) 
			{
				OnRemovePlayer();
			}
		}
			
		protected bool okToMove;

		public virtual void HorizontalPosition(float value)
		{
			if (Mathf.Abs(value) <= 0.1) 
			{
				okToMove = true;
			}

			if (okToMove) {
				if (Mathf.Abs(value) > 0.1) 
				{
					if (value < 0) 
					{
						OnLeft();
					} else 
					{
						OnRight();
					}
					okToMove = false;
				}
			}
		}

		public virtual void VerticalPosition(float value) { }
		public void RespawnActionDown() { }
		public virtual void AltActionReleased() { }
		public virtual void RespawnActionReleased() { }
		public void MobileJoystickPosition(Vector2 value) { }
		public virtual void LeftPressed() { }
		public virtual void RightPressed() { }
		public virtual void UpPressed() { }
		public virtual void DownPressed() { }
		public void AltActionPressed() { }
		public void RespawnActionPressed() { }
		public virtual void MainActionPressed() { }
		public void LeftReleased() { }
		public void RightReleased() { }
		public void UpReleased() { }
		public void DownReleased() { }
#endregion
	}
}